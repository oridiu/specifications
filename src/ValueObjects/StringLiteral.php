<?php

namespace Specifications\ValueObjects;

/**
 * Class StringLiteral.
 *
 * @author Thomas Joußen <tjoussen@databay.de>
 */
class StringLiteral
{

    /**
     * @var string
     */
    private $value;

    /**
     * StringLiteral constructor.
     *
     * @param string $value
     */
    public function __construct($value)
    {
        if (!is_string($value)) {
            throw new \InvalidArgumentException(\sprintf('The value %s is not a string', $value));
        }

        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->value;
    }
}
