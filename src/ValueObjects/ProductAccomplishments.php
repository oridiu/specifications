<?php

namespace Specifications\ValueObjects;

/**
 * Class ProductAccomplishments.
 *
 * @author Thomas Joußen <tjoussen@databay.de>
 */
final class ProductAccomplishments
{

    /**
     * @var StringLiteral
     */
    private $performance;

    /**
     * ProductAccomplishments constructor.
     *
     * @param StringLiteral $performance
     */
    public function __construct(StringLiteral $performance)
    {
        $this->performance = $performance;
    }

    /**
     * @param string $performance
     *
     * @return ProductUsage
     */
    public static function fromNative($performance)
    {
        return new self(
            new StringLiteral($performance)
        );
    }

    /**
     * @return StringLiteral
     */
    public function getPerformance()
    {
        return $this->performance;
    }
}