<?php

namespace Specifications\ValueObjects;

/**
 * Class DocumentCategory.
 *
 * @author Thomas Joußen <tjoussen@databay.de>
 */
final class DocumentCategory
{

    public static function get()
    {
        return [
            'client'                 => [],
            'timeAndBudget'          => [],
            'goal'                   => [
                'Zweck',
                'Nutzen',
            ],
            'productUsage'           => [
                'Anwendungsbereich(e)',
                'Zielgruppen, Anwender',
                'IST-Prozesse',
                'Unterstützte SOLL-Prozesse',
            ],
            'productFunctionality'   => [
                'Alle Funktionen, Eingabe/Ausgabe, beschrieben aus Anwendersicht',
                'Eingabe / Ausgabe detailliert',
            ],
            'productData'            => [
                'Mengengerüst',
                'Vorgaben für HW, SW, Schnitstellen',
            ],
            'productAccomplishments' => [
                'Performance, Zeitverhalten',
            ],
            'qualityStandard'        => [
                'Funktionalität',
                'Effizienz der Applikation',
                'Systemanforderung',
                'Zuverlässigkeit',
                'Skalierbarkeit und Erweiterbarkeit',
                'Anforderungen an die Sicherheit',
                'Anforderungen an die Hardware',
                'Life-Cycle',
            ],
            'comment'                => [],
            'glossary'               => [],
            'approval'               => [],
        ];
    }

    public static function getLabeled()
    {
        return [
            'Auftraggeber'            => [],
            'Zeit- und Budgetrahmen'  => [],
            'Zielabstimmung'          => [
                'Zweck',
                'Nutzen',
            ],
            'Produkteinsatz'          => [
                'Anwendungsbereich(e)',
                'Zielgruppen, Anwender',
                'IST-Prozesse',
                'Unterstützte SOLL-Prozesse',
            ],
            'Produktfunktionen'       => [
                'Alle Funktionen, Eingabe/Ausgabe, beschrieben aus Anwendersicht',
                'Eingabe / Ausgabe detailliert',
            ],
            'Produktdaten'            => [
                'Mengengerüst',
                'Vorgaben für HW, SW, Schnitstellen',
            ],
            'Produktleistungen'       => [
                'Performance, Zeitverhalten',
            ],
            'Qualitaetsanforderungen' => [
                'Funktionalität',
                'Effizienz der Applikation',
                'Systemanforderung',
                'Zuverlässigkeit',
                'Skalierbarkeit und Erweiterbarkeit',
                'Anforderungen an die Sicherheit',
                'Anforderungen an die Hardware',
                'Life-Cycle',
            ],
            'Anmerkungen'             => [],
            'Gloassar'                => [],
            'Lastenheftabnahme'       => [],
        ];
    }

}