<?php

namespace Specifications\ValueObjects;

/**
 * Class Dialogue.
 *
 * @author Thomas Joußen <tjoussen@databay.de>
 */
final class Dialogue
{

    private $id;

    /**
     * @var string
     */
    private $file;
}