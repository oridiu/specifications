<?php

namespace Specifications\ValueObjects;

/**
 * Class QualityStandard.
 *
 * @author Thomas Joußen <tjoussen@databay.de>
 */
final class QualityStandard
{

    /**
     * @var StringLiteral
     */
    private $functionality;

    /**
     * @var StringLiteral
     */
    private $efficiency;

    /**
     * @var StringLiteral
     */
    private $systemRequriements;

    /**
     * @var StringLiteral
     */
    private $reliability;

    /**
     * @var StringLiteral
     */
    private $scalability;

    /**
     * @var StringLiteral
     */
    private $security;

    /**
     * @var StringLiteral
     */
    private $hardware;

    /**
     * @var StringLiteral
     */
    private $lifeCycle;

    /**
     * QualityStandard constructor.
     *
     * @param StringLiteral $functionality
     * @param StringLiteral $efficiency
     * @param StringLiteral $systemRequriements
     * @param StringLiteral $reliability
     * @param StringLiteral $scalability
     * @param StringLiteral $security
     * @param StringLiteral $hardware
     * @param StringLiteral $lifeCycle
     */
    public function __construct(
        StringLiteral $functionality,
        StringLiteral $efficiency,
        StringLiteral $systemRequriements,
        StringLiteral $reliability,
        StringLiteral $scalability,
        StringLiteral $security,
        StringLiteral $hardware,
        StringLiteral $lifeCycle
    ) {
        $this->functionality      = $functionality;
        $this->efficiency         = $efficiency;
        $this->systemRequriements = $systemRequriements;
        $this->reliability        = $reliability;
        $this->scalability        = $scalability;
        $this->security           = $security;
        $this->hardware           = $hardware;
        $this->lifeCycle          = $lifeCycle;
    }

    /**
     * @return StringLiteral
     */
    public function getFunctionality()
    {
        return $this->functionality;
    }

    /**
     * @return StringLiteral
     */
    public function getEfficiency()
    {
        return $this->efficiency;
    }

    /**
     * @return StringLiteral
     */
    public function getSystemRequriements()
    {
        return $this->systemRequriements;
    }

    /**
     * @return StringLiteral
     */
    public function getReliability()
    {
        return $this->reliability;
    }

    /**
     * @return StringLiteral
     */
    public function getScalability()
    {
        return $this->scalability;
    }

    /**
     * @return StringLiteral
     */
    public function getSecurity()
    {
        return $this->security;
    }

    /**
     * @return StringLiteral
     */
    public function getHardware()
    {
        return $this->hardware;
    }

    /**
     * @return StringLiteral
     */
    public function getLifeCycle()
    {
        return $this->lifeCycle;
    }
}
