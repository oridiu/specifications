<?php

namespace Specifications\ValueObjects;

/**
 * Class Goal.
 *
 * @author Thomas Joußen <tjoussen@databay.de>
 */
final class Goal
{

    /**
     * @var StringLiteral
     */
    private $purpose;

    /**
     * @var StringLiteral
     */
    private $benefit;

    public function __construct(StringLiteral $purpose, StringLiteral $benefit)
    {
        $this->purpose = $purpose;
        $this->benefit = $benefit;
    }

    /**
     * @param string $purpose
     * @param string $benefit
     */
    public static function fromNative($purpose, $benefit)
    {
        return new self(
            new StringLiteral($purpose), new StringLiteral($benefit)
        );
    }

    /**
     * @return StringLiteral
     */
    public function getPurpose()
    {
        return $this->purpose;
    }

    /**
     * @return StringLiteral
     */
    public function getBenefit()
    {
        return $this->benefit;
    }
}
