<?php

namespace Specifications\Model;

use Specifications\ValueObjects\StringLiteral;

/**
 * Class ProductUsage.
 *
 * @author Thomas Joußen <tjoussen@databay.de>
 */
final class ProductUsage
{

    /**
     * @var StringLiteral
     */
    private $scope;

    /**
     * @var StringLiteral
     */
    private $stakeholer;

    /**
     * @var StringLiteral
     */
    private $asIsAnalysis;

    /**
     * @var StringLiteral
     */
    private $toBeRequirements;

    /**
     * ProductUsage constructor.
     *
     * @param StringLiteral $scope
     * @param StringLiteral $stakeholer
     * @param StringLiteral $asIsAnalysis
     * @param StringLiteral $toBeRequirements
     */
    public function __construct(
        StringLiteral $scope,
        StringLiteral $stakeholer,
        StringLiteral $asIsAnalysis,
        StringLiteral $toBeRequirements
    ) {
        $this->scope            = $scope;
        $this->stakeholer       = $stakeholer;
        $this->asIsAnalysis     = $asIsAnalysis;
        $this->toBeRequirements = $toBeRequirements;
    }

    /**
     * @param string $scope
     * @param string $stakeholder
     * @param string $asIsAnalysis
     * @param string $toBeRequirements
     *
     * @return ProductUsage
     */
    public static function fromNative($scope, $stakeholder, $asIsAnalysis, $toBeRequirements)
    {
        return new self(
            new StringLiteral($scope),
            new StringLiteral($stakeholder),
            new StringLiteral($asIsAnalysis),
            new StringLiteral($toBeRequirements)
        );
    }

    /**
     * @return StringLiteral
     */
    public function getScope()
    {
        return $this->scope;
    }

    /**
     * @return StringLiteral
     */
    public function getStakeholer()
    {
        return $this->stakeholer;
    }

    /**
     * @return StringLiteral
     */
    public function getAsIsAnalysis()
    {
        return $this->asIsAnalysis;
    }

    /**
     * @return StringLiteral
     */
    public function getToBeRequirements()
    {
        return $this->toBeRequirements;
    }

}