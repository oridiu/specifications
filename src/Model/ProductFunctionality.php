<?php

namespace Specifications\Model;

use Specifications\ValueObjects\StringLiteral;

/**
 * Class ProductFunctionality.
 *
 * @author Thomas Joußen <tjoussen@databay.de>
 */
final class ProductFunctionality
{

    /**
     * @var StringLiteral
     */
    private $descriptions;

    /**
     * @var StringLiteral
     */
    private $ioDetails;

    /**
     * ProductFunctionality constructor.
     *
     * @param StringLiteral $descriptions
     * @param StringLiteral $ioDetails
     */
    public function __construct(StringLiteral $descriptions, StringLiteral $ioDetails)
    {
        $this->descriptions = $descriptions;
        $this->ioDetails    = $ioDetails;
    }

    /**
     * @param string $description
     * @param string $ioDetails
     *
     * @return ProductUsage
     */
    public static function fromNative($description, $ioDetails)
    {
        return new self(
            new StringLiteral($description), new StringLiteral($ioDetails)
        );
    }

    /**
     * @return StringLiteral
     */
    public function getDescriptions()
    {
        return $this->descriptions;
    }

    /**
     * @return StringLiteral
     */
    public function getIoDetails()
    {
        return $this->ioDetails;
    }
}