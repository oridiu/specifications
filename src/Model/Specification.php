<?php

namespace Specifications\Model;

use Specifications\ValueObjects\Goal;
use Specifications\ValueObjects\ProductUsage;
use Specifications\ValueObjects\QualityStandard;
use Specifications\ValueObjects\StringLiteral;

/**
 * Class Specification.
 *
 * @author Thomas Joußen <tjoussen@databay.de>
 */
final class Specification
{

    /**
     * @var StringLiteral
     */
    private $id;

    /**
     * @var StringLiteral
     */
    private $title;

    /**
     * @var StringLiteral
     */
    private $client;

    /**
     * @var StringLiteral
     */
    private $timeAndBudget;

    /**
     * @var Goal
     */
    private $goal;

    /**
     * @var ProductUsage
     */
    private $productUsage;


    /**
     * @var StringLiteral
     */
    private $productFunctionality;

    /**
     * @var StringLiteral
     */
    private $productData;

    /**
     * @var StringLiteral
     */
    private $productAccomplishments;

    /**
     * @var QualityStandard
     */
    private $qualityStandard;

    /**
     * @var StringLiteral
     */
    private $comments;

    /**
     * Specification constructor.
     *
     * @param string $title
     */
    public function __construct($title)
    {
        $this->title = new StringLiteral($title);
    }

    /**
     * @return StringLiteral
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param StringLiteral $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return StringLiteral
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param StringLiteral $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return StringLiteral
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param StringLiteral $client
     */
    public function setClient($client)
    {
        $this->client = $client;
    }

    /**
     * @return StringLiteral
     */
    public function getTimeAndBudget()
    {
        return $this->timeAndBudget;
    }

    /**
     * @param StringLiteral $timeAndBudget
     */
    public function setTimeAndBudget($timeAndBudget)
    {
        $this->timeAndBudget = $timeAndBudget;
    }

    /**
     * @return Goal
     */
    public function getGoal()
    {
        return $this->goal;
    }

    /**
     * @param Goal $goal
     */
    public function setGoal($goal)
    {
        $this->goal = $goal;
    }

    /**
     * @return ProductUsage
     */
    public function getProductUsage()
    {
        return $this->productUsage;
    }

    /**
     * @param ProductUsage $productUsage
     */
    public function setProductUsage($productUsage)
    {
        $this->productUsage = $productUsage;
    }

    /**
     * @return StringLiteral
     */
    public function getProductFunctionality()
    {
        return $this->productFunctionality;
    }

    /**
     * @param StringLiteral $productFunctionality
     */
    public function setProductFunctionality($productFunctionality)
    {
        $this->productFunctionality = $productFunctionality;
    }

    /**
     * @return StringLiteral
     */
    public function getProductData()
    {
        return $this->productData;
    }

    /**
     * @param StringLiteral $productData
     */
    public function setProductData($productData)
    {
        $this->productData = $productData;
    }

    /**
     * @return StringLiteral
     */
    public function getProductAccomplishments()
    {
        return $this->productAccomplishments;
    }

    /**
     * @param StringLiteral $productAccomplishments
     */
    public function setProductAccomplishments($productAccomplishments)
    {
        $this->productAccomplishments = $productAccomplishments;
    }

    /**
     * @return QualityStandard
     */
    public function getQualityStandard()
    {
        return $this->qualityStandard;
    }

    /**
     * @param QualityStandard $qualityStandard
     */
    public function setQualityStandard($qualityStandard)
    {
        $this->qualityStandard = $qualityStandard;
    }

    /**
     * @return StringLiteral
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @param StringLiteral $comments
     */
    public function setComments($comments)
    {
        $this->comments = $comments;
    }

    public function setByChapter($chapter, $data)
    {
        $this->{$chapter} = $data;
    }

    public function getByChapter($chapter)
    {
        if (property_exists($this, $chapter)) {
            return $this->{$chapter};
        }

        return null;
    }
}
