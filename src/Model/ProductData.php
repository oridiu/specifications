<?php

namespace Specifications\Model;

use Specifications\ValueObjects\StringLiteral;

/**
 * Class ProductData.
 *
 * @author Thomas Joußen <tjoussen@databay.de>
 */
final class ProductData
{

    /**
     * @var StringLiteral
     */
    private $quantityStructure;

    /**
     * @var StringLiteral
     */
    private $interfaceRequirements;

    /**
     * ProductData constructor.
     *
     * @param StringLiteral $quantityStructure
     * @param StringLiteral $interfaceRequirements
     */
    public function __construct(StringLiteral $quantityStructure, StringLiteral $interfaceRequirements)
    {
        $this->quantityStructure     = $quantityStructure;
        $this->interfaceRequirements = $interfaceRequirements;
    }


    /**
     * @param string $quantityStructure
     * @param string $interfaceRequirements
     *
     * @return ProductUsage
     */
    public static function fromNative($quantityStructure, $interfaceRequirements)
    {
        return new self(
            new StringLiteral($quantityStructure), new StringLiteral($interfaceRequirements)
        );
    }

    /**
     * @return StringLiteral
     */
    public function getQuantityStructure()
    {
        return $this->quantityStructure;
    }

    /**
     * @return StringLiteral
     */
    public function getInterfaceRequirements()
    {
        return $this->interfaceRequirements;
    }
}