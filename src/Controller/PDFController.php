<?php

namespace Specifications\Controller;

use Specifications\Repository\SpecificationRepository;
use Specifications\Service\LatexToPdfConverter;
use Specifications\Service\TwigToLatexConverter;
use Specifications\ValueObjects\DocumentCategory;

/**
 * Class PDFController.
 *
 * @author Thomas Joußen <tjoussen@databay.de>
 */
final class PDFController
{

    /**
     * @var LatexToPdfConverter
     */
    private $latexConverter;

    /**
     * @var TwigToLatexConverter
     */
    private $twigConverter;

    /**
     * @var SpecificationRepository
     */
    private $repository;

    public function __construct(
        TwigToLatexConverter $twigConverter,
        LatexToPdfConverter $latexConverter,
        SpecificationRepository $repository
    ) {
        $this->twigConverter  = $twigConverter;
        $this->latexConverter = $latexConverter;
        $this->repository     = $repository;
    }

    public function generatePDF()
    {
        $specification = $this->repository->load($_GET['document']);

        $filename = $this->twigConverter->generate('document/index.tex.twig', $this->getTemplateData($specification));
        $file     = $this->latexConverter->generate($filename);

        return $file;
    }

    /**
     * @return array
     */
    private function getTemplateData($specification)
    {
        return [
            'chapters'      => DocumentCategory::get(),
            'specification' => $specification,
            'address'       => [
                'company'  => 'Databay AG',
                'street'   => 'Jens-Otto-Krag-Straße 11',
                'zip'      => '52146',
                'city'     => 'Würselen',
                'phone'    => '0 24 05/ 4 08 37-0',
                'fax'      => '024 05/ 4 08 37-0',
                'email'    => 'info@databay.de',
                'homepage' => 'https://www.datatabay.de',
            ],
            'title'         => 'Lastenheft',
            'subtitle'      => 'Erweiterungsprogrammierung für Ilias 5.2',
            'author'        => 'Thomas Joußen',
        ];
    }

}