<?php

namespace Specifications\Controller;

use Specifications\Form\ChosenType;
use Specifications\Form\TestType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormFactory;

/**
 * Class TestController.
 *
 * @author Thomas Joußen <tjoussen@databay.de>
 */
final class TestController
{

    /**
     * @var FormFactory
     */
    protected $formFactory;

    /**
     * @var \Twig_Environment
     */
    protected $twig;

    /**
     * TestController constructor.
     *
     * @param FormFactory       $formFactory
     * @param \Twig_Environment $twig
     */
    public function __construct(FormFactory $formFactory, \Twig_Environment $twig)
    {
        $this->formFactory = $formFactory;
        $this->twig        = $twig;
    }

    public function test()
    {
        $form = $this->formFactory->create(TestType::class);

        return $this->twig->render(
            'test.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

}