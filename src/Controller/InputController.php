<?php

namespace Specifications\Controller;

use Specifications\Form\SpecificationType;
use Specifications\Form\StringLiteralType;
use Specifications\Repository\SpecificationRepository;
use Specifications\ValueObjects\DocumentCategory;
use Symfony\Component\Form\FormFactory;

/**
 * Class InputController.
 *
 * @author Thomas Joußen <tjoussen@databay.de>
 */
final class InputController
{

    /**
     * @var \Twig_Environment
     */
    private $twig;

    /**
     * @var FormFactory
     */
    private $formFactory;

    /**
     * @var SpecificationRepository
     */
    private $repository;

    const DEFAULT_CHAPTER = 'client';

    /**
     * InputController constructor.
     *
     * @param \Twig_Environment $twig
     */
    public function __construct(\Twig_Environment $twig, FormFactory $formFactory, SpecificationRepository $repository)
    {
        $this->twig        = $twig;
        $this->formFactory = $formFactory;
        $this->repository  = $repository;
    }

    public function index()
    {
        $form = $this->formFactory->create(StringLiteralType::class);
        $form->handleRequest();

        if ($form->isSubmitted() && $form->isValid()) {
            $this->repository->save($form->getData());
        }

        $specifications = $this->repository->listIndex();

        return $this->twig->render(
            'ui/index.html.twig',
            [
                'specifications' => $specifications,
                'form'           => $form->createView(),
            ]
        );
    }

    public function upload()
    {
        return $this->repository->uploadImage('Test');
    }

    public function document()
    {
        $chapter       = self::DEFAULT_CHAPTER;
        $specification = $this->repository->load($_GET['document']);

        if (isset($_GET['chapter'])) {
            $chapter = $_GET['chapter'];
        }

        return $this->handleChapter($chapter, $specification);
    }

    private function handleChapter($chapter, $specification)
    {
        $form = $this->formFactory->create(
            SpecificationType::class,
            $specification,
            [
                'chapter' => $chapter,
            ]
        );
        $form->handleRequest();

        if ($form->isSubmitted() && $form->isValid()) {
            $this->repository->save($specification);
        }

        return $this->twig->render(
            'ui/document.html.twig',
            [
                'chapters' => DocumentCategory::get(),
                'form'     => $form->createView(),
                'document' => $_GET['document'],
            ]
        );
    }

}
