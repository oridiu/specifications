<?php

namespace Specifications\Form;

use Specifications\ValueObjects\ProductFunctionality;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\DataMapperInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;

/**
 * Class ProductFunctionalityType.
 *
 * @author Thomas Joußen <tjoussen@databay.de>
 */
final class ProductFunctionalityType extends AbstractType implements DataMapperInterface
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'descriptions',
            StringLiteralType::class,
            [
                'required' => false,
            ]
        )->add(
            'ioDetails',
            StringLiteralType::class,
            [
                'required' => false,
            ]
        )->setDataMapper($this);
    }

    /**
     * @param ProductFunctionality         $data
     * @param FormInterface[]|\Traversable $forms
     */
    public function mapDataToForms($data, $forms)
    {
        if ($data === null) {
            return;
        }

        $forms = iterator_to_array($forms);

        $forms['descriptions']->setData($data->getDescriptions());
        $forms['ioDetails']->setData($data->getIoDetails());
    }

    /**
     * @param FormInterface[]|\Traversable $forms
     * @param ProductFunctionality         $data
     */
    public function mapFormsToData($forms, &$data)
    {
        $forms = iterator_to_array($forms);

        $data = new ProductFunctionality(
            $forms['descriptions']->getData(), $forms['ioDetails']->getData()
        );
    }


}