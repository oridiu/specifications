<?php

namespace Specifications\Form;

use Specifications\Model\Specification;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\DataMapperInterface;
use Symfony\Component\Form\Exception;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;

/**
 * Class CreateSpecificationType.
 *
 * @author Thomas Joußen <tjoussen@databay.de>
 */
final class CreateSpecificationType extends AbstractType implements DataMapperInterface
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'title',
            TextType::class,
            [
                'required' => true,
            ]
        )->setDataMapper($this);
    }

    /**
     * @param Specification                $data
     * @param FormInterface[]|\Traversable $forms
     */
    public function mapDataToForms($data, $forms)
    {
        $forms = iterator_to_array($forms);

        if ($data === null) {
            return;
        }

        $forms['title']->setData($data->getTitle());
    }

    /**
     * @param FormInterface[]|\Traversable $forms
     * @param Specification                $data
     */
    public function mapFormsToData($forms, &$data)
    {
        $forms = iterator_to_array($forms);

        $data = new Specification(
            $forms['title']->getData()
        );
    }


}