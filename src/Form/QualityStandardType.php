<?php

namespace Specifications\Form;

use Specifications\ValueObjects\QualityStandard;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\DataMapperInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;

/**
 * Class QualityStandardType.
 *
 * @author Thomas Joußen <tjoussen@databay.de>
 */
final class QualityStandardType extends AbstractType implements DataMapperInterface
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
                'functionality',
                StringLiteralType::class,
                [
                    'required' => false,
                ]
            )->add(
                'efficiency',
                StringLiteralType::class,
                [
                    'required' => false,
                ]
            )->add(
                'systemRequirements',
                StringLiteralType::class,
                [
                    'required' => false,
                ]
            )->add(
                'reliability',
                StringLiteralType::class,
                [
                    'required' => false,
                ]
            )->add(
                'scalability',
                StringLiteralType::class,
                [
                    'required' => false,
                ]
            )->add(
                'security',
                StringLiteralType::class,
                [
                    'required' => false,
                ]
            )->add(
                'hardware',
                StringLiteralType::class,
                [
                    'required' => false,
                ]
            )->add(
                'lifeCycle',
                StringLiteralType::class,
                [
                    'required' => false,
                ]
            )->setDataMapper($this);
    }

    /**
     * @param QualityStandard              $data
     * @param FormInterface[]|\Traversable $forms
     */
    public function mapDataToForms($data, $forms)
    {
        if ($data === null) {
            return;
        }

        $forms = iterator_to_array($forms);

        $forms['functionality']->setData($data->getFunctionality());
        $forms['efficiency']->setData($data->getEfficiency());
        $forms['systemRequirements']->setData($data->getSystemRequriements());
        $forms['reliability']->setData($data->getReliability());
        $forms['scalability']->setData($data->getScalability());
        $forms['security']->setData($data->getSecurity());
        $forms['hardware']->setData($data->getHardware());
        $forms['lifeCycle']->setData($data->getLifeCycle());
    }

    /**
     * @param FormInterface[]|\Traversable $forms
     * @param QualityStandard              $data
     */
    public function mapFormsToData($forms, &$data)
    {
        $forms = iterator_to_array($forms);

        $data = new QualityStandard(
            $forms['functionality']->getData(),
            $forms['efficiency']->getData(),
            $forms['systemRequirements']->getData(),
            $forms['reliability']->getData(),
            $forms['scalability']->getData(),
            $forms['security']->getData(),
            $forms['hardware']->getData(),
            $forms['lifeCycle']->getData()
        );
    }


}