<?php

namespace Specifications\Form;

use Specifications\Model\Specification;
use Specifications\ValueObjects\ProductAccomplishments;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class SpecificationType.
 *
 * @author Thomas Joußen <tjoussen@databay.de>
 */
final class SpecificationType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        switch ($options['chapter']) {
            case 'client':
                $builder->add('client', StringLiteralType::class);
                break;
            case 'timeAndBudget':
                $builder->add('timeAndBudget', StringLiteralType::class);
                break;
            case 'goal':
                $builder->add('goal', GoalType::class);
                break;
            case 'qualityStandard':
                $builder->add('qualityStandard', QualityStandardType::class);
                break;
            case 'productUsage':
                $builder->add('productUsage', ProductUsageType::class);
                break;
            case 'productFunctionality':
                $builder->add('productFunctionality', ProductFunctionalityType::class);
                break;
            case 'productData':
                $builder->add('productData', ProductDataType::class);
                break;
            case 'productAccomplishments':
                $builder->add('productAccomplishments', ProductAccomplishmentsType::class);
                break;
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => Specification::class,
            ]
        );

        $resolver->setDefined(
            [
                'chapter',
            ]
        );
    }

}
