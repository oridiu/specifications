<?php

namespace Specifications\Form;

use Specifications\ValueObjects\StringLiteral;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class StringLiteralType.
 *
 * @author Thomas Joußen <tjoussen@databay.de>
 */
class StringLiteralType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'template',
            ChoiceType::class,
            [
                'chosen'            => true,
                'mapped'            => false,
                'choices'           => [
                    "Template1",
                    "Template2",
                    "Template3",
                ],
                'choices_as_values' => true,
                'choice_label'      => function ($choice) {
                    return $choice;
                },
            ]
        )->add(
            'value',
            TextareaType::class,
            [
                'markdown'     => true,
                'inherit_data' => true,
            ]
        );
        $builder->addModelTransformer(
            new CallbackTransformer(
                function ($data) {
                    return new StringLiteral((string)$data);
                }, function ($data) {
                return new StringLiteral((string)$data);
            }
            )
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => StringLiteral::class,
            ]
        );
    }
}