<?php

namespace Specifications\Form;

use Specifications\ValueObjects\ProductUsage;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\DataMapperInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;

/**
 * Class ProductUsageType.
 *
 * @author Thomas Joußen <tjoussen@databay.de>
 */
final class ProductUsageType extends AbstractType implements DataMapperInterface
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
                'scope',
                StringLiteralType::class,
                [
                    'required' => false,
                ]
            )->add(
                'stakeholder',
                StringLiteralType::class,
                [
                    'required' => false,
                ]
            )->add(
                'asIsAnalysis',
                StringLiteralType::class,
                [
                    'required' => false,
                ]
            )->add(
                'toBeRequirements',
                StringLiteralType::class,
                [
                    'required' => false,
                ]
            )->setDataMapper($this);
    }

    /**
     * @param ProductUsage                 $data
     * @param FormInterface[]|\Traversable $forms
     */
    public function mapDataToForms($data, $forms)
    {
        if ($data === null) {
            return;
        }

        $forms = iterator_to_array($forms);

        $forms['scope']->setData($data->getScope());
        $forms['stakeholder']->setData($data->getStakeholer());
        $forms['asIsAnalysis']->setData($data->getAsIsAnalysis());
        $forms['toBeRequirements']->setData($data->getToBeRequirements());
    }

    /**
     * @param FormInterface[]|\Traversable $forms
     * @param ProductUsage                 $data
     */
    public function mapFormsToData($forms, &$data)
    {
        $forms = iterator_to_array($forms);

        $data = new ProductUsage(
            $forms['scope']->getData(),
            $forms['stakeholder']->getData(),
            $forms['asIsAnalysis']->getData(),
            $forms['toBeRequirements']->getData()
        );
    }


}
