<?php

namespace Specifications\Form;

use Specifications\ValueObjects\ProductAccomplishments;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\DataMapperInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;

/**
 * Class ProductAccomplishmentsType.
 *
 * @author Thomas Joußen <tjoussen@databay.de>
 */
final class ProductAccomplishmentsType extends AbstractType implements DataMapperInterface
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'performance',
            StringLiteralType::class,
            [
                'required' => false,
            ]
        )->setDataMapper($this);
    }

    /**
     * @param ProductAccomplishments       $data
     * @param FormInterface[]|\Traversable $forms
     */
    public function mapDataToForms($data, $forms)
    {
        if ($data === null) {
            return;
        }

        $forms = iterator_to_array($forms);

        $forms['performance']->setData($data->getPerformance());
    }

    /**
     * @param FormInterface[]|\Traversable $forms
     * @param ProductAccomplishments       $data
     */
    public function mapFormsToData($forms, &$data)
    {
        $forms = iterator_to_array($forms);

        $data = new ProductAccomplishments(
            $forms['performance']->getData()
        );
    }


}
