<?php

namespace Specifications\Form;

use Specifications\Model\Specification;
use Specifications\ValueObjects\Goal;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\DataMapperInterface;
use Symfony\Component\Form\Exception;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ClientType.
 *
 * @author Thomas Joußen <tjoussen@databay.de>
 */
final class GoalType extends AbstractType implements DataMapperInterface
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
                'purpose',
                StringLiteralType::class,
                [
                    'required' => false,
                ]
            )->add(
                'benefit',
                StringLiteralType::class,
                [
                    'required' => false,
                ]
            )->setDataMapper($this);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
    }

    /**
     * @param Goal                         $data
     * @param FormInterface[]|\Traversable $forms
     */
    public function mapDataToForms($data, $forms)
    {
        if ($data === null) {
            return;
        }

        $forms = iterator_to_array($forms);

        $forms['purpose']->setData($data->getPurpose());
        $forms['benefit']->setData($data->getBenefit());
    }

    /**
     * @param FormInterface[]|\Traversable $forms
     * @param Goal                         $data
     */
    public function mapFormsToData($forms, &$data)
    {
        $forms = iterator_to_array($forms);

        $data = new Goal($forms['purpose']->getData(), $forms['benefit']->getData());
    }


}
