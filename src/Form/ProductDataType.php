<?php

namespace Specifications\Form;

use Specifications\ValueObjects\ProductData;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\DataMapperInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;

/**
 * Class ProductDataType.
 *
 * @author Thomas Joußen <tjoussen@databay.de>
 */
final class ProductDataType extends AbstractType implements DataMapperInterface
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'quantityStructure',
            StringLiteralType::class,
            [
                'required' => false,
            ]
        )->add(
            'interfaceRequirements',
            StringLiteralType::class,
            [
                'required' => false,
            ]
        )->setDataMapper($this);
    }

    /**
     * @param ProductData                  $data
     * @param FormInterface[]|\Traversable $forms
     */
    public function mapDataToForms($data, $forms)
    {
        if ($data === null) {
            return;
        }

        $forms = iterator_to_array($forms);

        $forms['quantityStructure']->setData($data->getQuantityStructure());
        $forms['interfaceRequirements']->setData($data->getQuantityStructure());
    }

    /**
     * @param FormInterface[]|\Traversable $forms
     * @param ProductData                  $data
     */
    public function mapFormsToData($forms, &$data)
    {
        $forms = iterator_to_array($forms);

        $data = new ProductData(
            $forms['quantityStructure']->getData(), $forms['interfaceRequirements']->getData()
        );
    }


}