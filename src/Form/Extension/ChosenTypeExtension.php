<?php

namespace Specifications\Form\Extension;

use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ChosenTypeExtension.
 *
 * @author Thomas Joußen <tjoussen@databay.de>
 */
final class ChosenTypeExtension extends AbstractTypeExtension
{

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        if (isset($options['chosen']) && $options['chosen'] === true) {
            $view->vars['chosen'] = true;
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefined(
            [
                'chosen',
            ]
        );
    }

    public function getExtendedType()
    {
        return ChoiceType::class;
    }
}