<?php

namespace Specifications\Form\Extension;

use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class MarkdownTypeExtension.
 *
 * @author Thomas Joußen <tjoussen@databay.de>
 */
final class MarkdownTypeExtension extends AbstractTypeExtension
{

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        if (isset($options['markdown']) && $options['markdown'] === true) {
            $view->vars['markdown'] = true;
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefined(
            [
                'markdown',
            ]
        );
    }

    public function getExtendedType()
    {
        return TextareaType::class;
    }
}