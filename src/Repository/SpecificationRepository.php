<?php

namespace Specifications\Repository;

use Specifications\Model\Specification;

/**
 * Class SpecificationRepository.
 *
 * @author Thomas Joußen <tjoussen@databay.de>
 */
final class SpecificationRepository
{

    /**
     * @var string
     */
    private $outputDir;

    /**
     * SpecificationRepository constructor.
     *
     * @param string $outputDir
     */
    public function __construct($outputDir)
    {
        $this->outputDir = $outputDir;
    }

    /**
     * @param string $name
     *
     * @return Specification
     */
    public function load($name)
    {
        $data = file_get_contents(\sprintf('%s/%s/Specification', $this->outputDir, $name));

        return unserialize($data);
    }

    /**
     * @param Specification $specification
     */
    public function save(Specification $specification)
    {
        $filename = $specification->getTitle();
        $path     = \sprintf('%s/%s/Specification', $this->outputDir, $filename);

        if (!is_dir(dirname($path))) {
            mkdir(dirname($path), '2775');
        }

        file_put_contents($path, serialize($specification));
    }

    public function uploadImage($name)
    {
        $uploadFolder = \sprintf('%s/%s/', $this->outputDir, $name);
        $onlinePath   = $uploadFolder;
        $response     = [];
        if (isset($_FILES['file'])) {
            $file     = $_FILES['file'];
            $filename = uniqid().'.'.(pathinfo($file['name'], PATHINFO_EXTENSION) ?: 'png');
            move_uploaded_file($file['tmp_name'], $uploadFolder.$filename);
            $response['filename'] = $onlinePath.$filename;
        } else {
            $response['error'] = 'Error while uploading file';
        }

        return json_encode($response);
    }

    public function listIndex()
    {
        $data = scandir($this->outputDir);
        $data = array_filter(
            $data,
            function ($element) {
                return !in_array($element, ['.', '..']);
            }
        );

        return $data;
    }

}