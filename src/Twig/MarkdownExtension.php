<?php

namespace Specifications\Twig;

use cebe\markdown\latex\GithubMarkdown;

/**
 * Class MarkdownExtension.
 *
 * @author Thomas Joußen <tjoussen@databay.de>
 */
final class MarkdownExtension extends \Twig_Extension
{

    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('toLatex', [$this, 'toLatex']),
        ];
    }

    public function toLatex($content)
    {
        $parser = new GithubMarkdown();

        $content = $parser->parse($content);

        return $content;
    }
}