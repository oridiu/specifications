<?php

namespace Specifications\Service;

use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

/**
 * Class PDFService.
 *
 * @author Thomas Joußen <tjoussen@databay.de>
 */
final class LatexToPdfConverter
{

    /**
     * @var string
     */
    private $binary = '/usr/bin/pdflatex';

    /**
     * @var string
     */
    private $outputDir = '/var/www/tjoussen/specifications/output';

    /**
     * LatexToPdfConverter constructor.
     *
     * @param string $binary
     * @param string $outputDir
     */
    public function __construct($binary, $outputDir)
    {
        $this->binary    = $binary;
        $this->outputDir = $outputDir;
    }

    public function generate($file)
    {
        $process = new Process(
            \sprintf(
                '%s -file-line-error -interaction=nonstopmode -synctex=1 -output-format=pdf -output-directory=%s %s',
                $this->binary,
                $this->outputDir,
                $file
            )
        );
        $process->run();

        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        // Re run process to include table of contents etc.
        $process->run();

        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        $filename = basename($file);

        unlink($this->outputDir.'/'.\str_replace('.tex', '.aux', $filename));
        unlink($this->outputDir.'/'.\str_replace('.tex', '.synctex.gz', $filename));
        unlink($this->outputDir.'/'.\str_replace('.tex', '.toc', $filename));
        unlink($this->outputDir.'/'.\str_replace('.tex', '.out', $filename));
        unlink($this->outputDir.'/'.\str_replace('.tex', '.log', $filename));

        return $process->getOutput();
    }
}
