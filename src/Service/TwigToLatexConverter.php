<?php

namespace Specifications\Service;

/**
 * Class TwigToLatexConverter.
 *
 * @author Thomas Joußen <tjoussen@databay.de>
 */
final class TwigToLatexConverter
{

    /**
     * @var \Twig_Environment
     */
    private $twig;

    /**
     * @var String
     */
    private $outputDir;

    /**
     * TwigToLatexConverter constructor.
     *
     * @param \Twig_Environment $twig
     * @param string            $outputDir
     */
    public function __construct(\Twig_Environment $twig, $outputDir)
    {
        $this->twig      = $twig;
        $this->outputDir = $outputDir;
    }

    public function generate($template, $data)
    {
        $output   = $this->twig->render($template, $data);
        $filename = \sprintf('%s/%s.tex', $this->outputDir, uniqid('tex_'));

        if (file_exists($filename)) {
            unlink($filename);
        }

        file_put_contents($filename, $output);

        return $filename;
    }
}