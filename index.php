<?php

require_once 'vendor/autoload.php';


// the Twig file that holds all the default markup for rendering forms
// this file comes with TwigBridge
$defaultFormTheme = 'bootstrap_3_layout.html.twig';

$vendorDir = realpath(__DIR__.'/../vendor');
// the path to TwigBridge library so Twig can locate the
// form_div_layout.html.twig file
$appVariableReflection = new \ReflectionClass('\Symfony\Bridge\Twig\AppVariable');
$vendorTwigBridgeDir = dirname($appVariableReflection->getFileName());
// the path to your other templates
$viewsDir = 'src/Resources/templates';

$loader = new Twig_Loader_Filesystem([
    $viewsDir,
    $vendorTwigBridgeDir.'/Resources/views/Form',
]);
$twig = new Twig_Environment($loader, array(
   // 'cache' => 'cache/twig',
));
$formEngine = new \Symfony\Bridge\Twig\Form\TwigRendererEngine(array($defaultFormTheme, 'fields.html.twig'), $twig);
$twig->addRuntimeLoader(new \Twig_FactoryRuntimeLoader(array(
   \Symfony\Bridge\Twig\Form\TwigRenderer::class => function () use ($formEngine, $csrfManager) {
       return new \Symfony\Bridge\Twig\Form\TwigRenderer($formEngine, $csrfManager);
   },
)));
$twig->addExtension(new \Symfony\Bridge\Twig\Extension\FormExtension());
$twig->addExtension(new \Specifications\Twig\MarkdownExtension());


// create the Translator
$translator = new \Symfony\Component\Translation\Translator('en');
// somehow load some translations into it
$translator->addLoader('yml', new \Symfony\Component\Translation\Loader\YamlFileLoader());
$translator->addResource(
    'yml',
    __DIR__.'/src/Resources/translations/messages.en.yml',
    'en'
);
$translator->setFallbackLocales(['en']);
// add the TranslationExtension (gives us trans and transChoice filters)
$twig->addExtension(new \Symfony\Bridge\Twig\Extension\TranslationExtension($translator));

$formFactoryBuilder = \Symfony\Component\Form\Forms::createFormFactoryBuilder();
$formFactoryBuilder->addTypeExtensions(
    [
        new Specifications\Form\Extension\ChosenTypeExtension(),
        new Specifications\Form\Extension\MarkdownTypeExtension(),
    ]
);
$formFactory = $formFactoryBuilder->getFormFactory();

$twigConverter  = new \Specifications\Service\TwigToLatexConverter($twig, 'output/latex');
$latexConverter = new \Specifications\Service\LatexToPdfConverter('/usr/bin/pdflatex', 'output/pdf');

$repository = new \Specifications\Repository\SpecificationRepository('output/repository');


$pdfController = new \Specifications\Controller\PDFController($twigConverter, $latexConverter, $repository);
$inputController = new \Specifications\Controller\InputController($twig, $formFactory, $repository);

if($_GET['action'] == 'generatePDF')
{
    echo $pdfController->generatePDF();
} elseif($_GET['action']) {
    $action = $_GET['action'];
    echo $inputController->$action();
} else {
    echo $inputController->index();
}

